import { Component } from '@angular/core';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  constructor() {}
 async scanning(){
  try {
    const scan = await navigator.bluetooth.requestLEScan({acceptAllAdvertisements :true});

    console.log('Scan started with:');
    console.log(' acceptAllAdvertisements: ' + scan.acceptAllAdvertisements);
    console.log(' active: ' + scan.active);
    console.log(' keepRepeatedDevices: ' + scan.keepRepeatedDevices);

    navigator.bluetooth.addEventListener('advertisementreceived', event => {
      var company = null;
      event.manufacturerData.forEach(function(value, key, map){
        company = [value, key]
      });
      if (company[1]==1177 && company[0].getUint8(0)==3){
        console.log("Ruuvi Tag Found")
        console.log('Advertisement received.');
        console.log('  Device Name: ' + event.device.name);
        console.log('  Device ID: ' + event.device.id);
        console.log('  RSSI: ' + event.rssi);
        console.log('  TX Power: ' + event.txPower); 
        console.log('  Data Mode: ' + company[0].getUint8(0));
        console.log("  Humidity : " + company[0].getUint8(1)/2 + "RH%");
        console.log("  TemperatureC: " + (company[0].getInt8(2) + company[0].getInt8(3)/100 ) + "°C");
        var pressure = ((parseInt(company[0].getUint8(4).toString(16) + company[0].getUint8(5).toString(16), 16) + 50000)/101325).toFixed(3);
        console.log("  Pressure: " + pressure + " atm");

        var accX = company[0].getUint8(6).toString(16) + company[0].getUint8(7).toString(16)
        if (accX.length%2 !=0){
          accX = "0" + accX
        }
        accX = parseInt(accX, 16)
        if (accX > 32767) { accX =  accX - 65536 }
        console.log("  AccX: " + accX + "mG")

        var accY = company[0].getUint8(8).toString(16) + company[0].getUint8(9).toString(16);
        if (accY.length%2 !=0){
          accY = "0" + accY
        }
        accY = parseInt(accY, 16)
        if (accY > 32767) { accY =  accY - 65536 }
        console.log("  AccY: " + accY + "mG")

        var accZ = company[0].getUint8(10).toString(16) + company[0].getUint8(11).toString(16);
        if (accZ.length%2 !=0){
          accZ = "0" + accZ
        }
        accZ = parseInt(accZ, 16)
        if (accZ > 32767) { accZ =  accZ - 65536 }
        console.log("  AccZ: " + accZ + "mG")

      }

      
    });

    setTimeout(stopScan, 10000);
    function stopScan() {
      console.log('Stopping scan...');
      scan.stop();
      console.log('Stopped.  scan.active = ' + scan.active);
    }
  } catch(error)  {
    console.log(error);
  }
}
 

}
