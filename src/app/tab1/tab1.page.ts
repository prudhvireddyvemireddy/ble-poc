import { Component } from '@angular/core';
import { BleClient, numberToUUID } from '@capacitor-community/bluetooth-le';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  SERVICE = numberToUUID(0xfeaa);

  constructor() {}
  ngOnInit(){
  }
  
  async scan(){
    try {
      console.log("asdfasdf")
      await BleClient.initialize();
  
      await BleClient.requestLEScan(
        {
          
        },
        result => {
          console.log('received new scan result', result);
        },
      );
  
      setTimeout(async () => {
        await BleClient.stopLEScan();
        console.log('stopped scanning');
      }, 5000);
    } catch (error) {
      console.error(error);
    }
  }
}


